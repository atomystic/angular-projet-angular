import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardAvocatComponent } from './board-avocat/board-avocat.component';
import { BoardJuridiqueComponent } from './board-juridique/board-juridique.component';
import { AffaireAccueilComponent } from './affaire-accueil/affaire-accueil.component';
import { AffaireAjoutComponent } from './affaire-accueil/affaire-ajout/affaire-ajout/affaire-ajout.component';
import { DetailsTacheComponent } from './tache/details-tache/details-tache.component';




const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'avocat', component: BoardAvocatComponent },
  { path: 'juridique', component: BoardJuridiqueComponent },
  { path: "affaireHome", component: AffaireAccueilComponent },
  { path: "affaireAjout", component: AffaireAjoutComponent },
  { path: "detailsTache", component: DetailsTacheComponent },




  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
