import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TacheModel } from '../model/tache-model';

const API_URL = 'http://localhost:8081/rest/api/test/';

@Injectable({
  providedIn: 'root'
})
export class TacheServiceService  {

  constructor(private http: HttpClient) { }

  getTaches(): Observable<TacheModel[]> {
    
    return this.http.get<TacheModel[]>(API_URL + 'getTaches');
  }

  updateTache(tache:TacheModel): Observable<TacheModel> {
    
    return this.http.put<TacheModel>(API_URL + 'updateTache',tache);
  }

  addTache(tache:TacheModel,titreAffaire:string): Observable<TacheModel> {
    
    return this.http.post<TacheModel>(API_URL + 'addTache/'+titreAffaire,tache);
  }

  deleteTache(id:number){
    return this.http.delete(API_URL + "deleteTache/"+id,{observe:'response'});
  }

  getTacheById(id :number) : Observable<TacheModel>{

 
    return this.http.get<TacheModel>(API_URL+"getTacheById/"+id)
  } 
}