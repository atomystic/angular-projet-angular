import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8081/rest/api/test/';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getAvocatBoard(): Observable<any> {
    return this.http.get(API_URL + 'avocat', { responseType: 'text' });
  }

  getJuridiqueBoard(): Observable<any> {
    return this.http.get(API_URL + 'juridique', { responseType: 'text' });
  }

}