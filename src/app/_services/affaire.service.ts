import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Affaire } from '../model/affaire';
import { Observable } from 'rxjs';


const API_URL = "http://localhost:8081/rest/api/test/"
@Injectable({
  providedIn: 'root'
})
export class AffaireService {

  constructor(private http: HttpClient) { }

  getAllAffaires(): Observable<Affaire[]> {

    return this.http.get<Affaire[]>(API_URL + "getAllAffaire")
  }


  addAffaire(affaire: Affaire): Observable<Affaire> {

    return this.http.post<Affaire>(API_URL + "addAffaire", affaire)
  }
}
