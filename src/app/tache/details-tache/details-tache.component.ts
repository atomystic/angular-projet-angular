import { Component, OnInit, Input } from '@angular/core';
import { Event, ActivatedRoute } from '@angular/router';
import { TacheServiceService } from 'src/app/_services/tache-service.service';
import { TacheModel } from 'src/app/model/tache-model';

@Component({
  selector: 'app-details-tache',
  templateUrl: './details-tache.component.html',
  styleUrls: ['./details-tache.component.css']
})
export class DetailsTacheComponent implements OnInit {

tache :TacheModel;
  constructor(private ar :ActivatedRoute, private tacheService :TacheServiceService ) { }

  ngOnInit(): void {

    this.ar.queryParamMap.subscribe(idParam=>{
   let id = parseInt(idParam.get("id"))
   if(id !=undefined){
    this.tacheService.getTacheById(id).subscribe((tacheOut)=>{this.tache=tacheOut})
   }



    })








}




}
