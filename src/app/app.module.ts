import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin, {Draggable} from '@fullcalendar/interaction';
import { formatDate } from '@fullcalendar/angular';
import timeGridPlugin from "@fullcalendar/timegrid";
import momentTimezonePlugin from '@fullcalendar/moment-timezone';
import list from "@fullcalendar/list";



FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin,
  timeGridPlugin,
  list,
  momentTimezonePlugin,
]);




import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardJuridiqueComponent } from './board-juridique/board-juridique.component';
import { BoardAvocatComponent } from './board-avocat/board-avocat.component';
import { RouterModule } from '@angular/router';
import { DatePipePipe } from './_pipe/date-pipe.pipe';
import { AffaireAccueilComponent } from './affaire-accueil/affaire-accueil.component';
import { AffaireAjoutComponent } from './affaire-accueil/affaire-ajout/affaire-ajout/affaire-ajout.component';
import { DetailsTacheComponent } from './tache/details-tache/details-tache.component';





// import { authInterceptorProviders } from './_helpers/auth.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardJuridiqueComponent,
    BoardAvocatComponent,
    DatePipePipe,
    AffaireAccueilComponent,
    AffaireAjoutComponent,
    DetailsTacheComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FullCalendarModule,


    
  
  ],
  // providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
