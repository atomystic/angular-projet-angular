import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-avocat',
  templateUrl: './board-avocat.component.html',
  styleUrls: ['./board-avocat.component.css']
})
export class BoardAvocatComponent implements OnInit {
  content = '';
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAvocatBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }
}