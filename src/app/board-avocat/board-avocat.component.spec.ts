import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardAvocatComponent } from './board-avocat.component';

describe('BoardAvocatComponent', () => {
  let component: BoardAvocatComponent;
  let fixture: ComponentFixture<BoardAvocatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardAvocatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardAvocatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
