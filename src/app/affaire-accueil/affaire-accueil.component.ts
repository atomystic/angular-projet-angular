import { Component, OnInit } from '@angular/core';
import { Affaire } from '../model/affaire';
import {AffaireService} from  "../_services/affaire.service"

@Component({
  selector: 'app-affaire-accueil',
  templateUrl: './affaire-accueil.component.html',
  styleUrls: ['./affaire-accueil.component.css']
})
export class AffaireAccueilComponent implements OnInit {

  affaires :Affaire [];

  constructor(private affaireservice  :AffaireService) { }

  ngOnInit(): void {

this.affaireservice.getAllAffaires().subscribe((listaffaire)=>{
this.affaires=listaffaire;
console.log(this.affaires)

})
  }

}
