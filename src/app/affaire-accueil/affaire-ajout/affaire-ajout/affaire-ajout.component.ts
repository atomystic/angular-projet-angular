import { Component, OnInit } from '@angular/core';
import { AffaireService } from 'src/app/_services/affaire.service';
import { Affaire } from 'src/app/model/affaire';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-affaire-ajout',
  templateUrl: './affaire-ajout.component.html',
  styleUrls: ['./affaire-ajout.component.css']
})
export class AffaireAjoutComponent implements OnInit {

  affaire: Affaire = new Affaire();

  constructor(private affaireAjoutService: AffaireService, private route :Router) { }

  ngOnInit(): void {
  }

  addAffaire() {

    console.log(this.affaire)
    this.affaireAjoutService.addAffaire(this.affaire).subscribe((data)=>{

      this.route.navigateByUrl("affaireHome")

    })

  }

}
