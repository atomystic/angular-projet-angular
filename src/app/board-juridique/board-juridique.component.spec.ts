import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardJuridiqueComponent } from './board-juridique.component';

describe('BoardJuridiqueComponent', () => {
  let component: BoardJuridiqueComponent;
  let fixture: ComponentFixture<BoardJuridiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardJuridiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardJuridiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
