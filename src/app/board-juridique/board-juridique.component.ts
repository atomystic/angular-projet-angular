import { Component, OnInit, AfterContentChecked, InjectionToken, NgZone, ElementRef, AfterViewInit, ViewChild, AfterViewChecked, ViewChildren, ɵConsole, Output, EventEmitter } from '@angular/core';
import { CalendarOptions, Calendar } from '@fullcalendar/angular';
import { Router } from '@angular/router';
import { TacheServiceService } from '../_services/tache-service.service';
import { TacheModel } from '../model/tache-model';
import { Draggable } from '@fullcalendar/interaction';
import { EventManager } from '@angular/platform-browser';
import * as moment from 'moment-timezone';
import { Affaire } from '../model/affaire';
import { AffaireService } from '../_services/affaire.service';



@Component({
  selector: 'app-board-juridique',
  templateUrl: './board-juridique.component.html',
  styleUrls: ['./board-juridique.component.css']
})
export class BoardJuridiqueComponent implements OnInit {

  // ATTRIBUTS DE CLASSE


  tache: TacheModel;
  taches : TacheModel[];
  titreAffaire: string;
  listAffaires: Affaire[];
  start: string;
  end: string;
  tacheToAdd: TacheModel = new TacheModel();
  calendarOptions: CalendarOptions;
  displayCalendar: boolean = true;
  @ViewChild("modalBtn") modalBtn: ElementRef;
  @ViewChild("modalBg") modalBg: ElementRef;
  @ViewChild("modalClose") modalClose: ElementRef;


  // eventToAddTag = document.getElementById('mydraggable');
  // calendarTag = document.getElementById('mycalendar');

  // draggableEvenToAdd: Draggable = new Draggable(this.eventToAddTag);
  // draggableCalendar: Draggable = new Draggable(this.calendarTag, { itemSelector: ".item-class" });


  //CONSTRUCTEURS
  constructor(private rt: Router, private tacheService: TacheServiceService, private affaireService: AffaireService) { }

  //METHODES NGONINIT
  ngOnInit(): void {


    //CHARGER LA LISTE DE TACHE DE LA BASE DONNE
    this.tacheService.getTaches().subscribe(data => {
      this.taches = data;

      //Construction du calendrier 
      let calendrier = {
        initialView: "timeGridWeek",
        locale: "fr",
        headerToolbar: {
          start: 'prev,next today',
          center: 'title',
          end: 'dayGridMonth,timeGridWeek,list'
        },
        buttonText: {
          today: "Aujourd'hui",
          month: "Mois",
          week: "Semaine",
          list: "Liste"

        },
        nowIndicator: true,
        events: this.taches,
        editable: true,
        eventDrop: (infosTache) => {
          this.updateTache(infosTache);
        },
        timeZone: "Europe/Paris",
        eventResize: (infosTache) => {
          this.updateTache(infosTache);
        },
        // droppable: true,
        selectable: true, // on peut sdélectionner une page horaire
        selectMirror: true, // ajoute un placeholder
        unselectAuto: true, // si nclique en dehors de la selection, prochaine selection est nettoyer
        select: (infos) => {


          // moment.tz(my_date, "America/Argentina/Buenos_Aires").format("YYYY-MM-DDTHH:mm:ss.SSSZ")
          // var newYork    = moment.tz("2014-06-01 12:00", "America/New_Yor  
          this.displayForm(infos)
        },
        eventClick: (infos) => {
          this.deleteTache(infos)

        },

        eventMouseEnter: () => {

        },

        // dayMaxEventRows:1,



      }//calendrier
      this.calendarOptions = calendrier;
    })// fin de l'appel de la méthode getAllTaches


    // récupérer les affairs pour la liste déroulante
    this.affaireService.getAllAffaires().subscribe((data) => {

      this.listAffaires = data
    })


  }// NGONINIT

  // METHODE DU CRUD
  updateTache(infosTache) {
    this.tache = {
      idTache: infosTache.event.extendedProps.idTache,
      start: infosTache.event.start,
      title: infosTache.event.title,
      end: infosTache.event.end,
      description: "",
      statutAudience: true,
      affaire : infosTache.event.extendedProps.affaire

    }
    if (confirm("Etes vous sûr.e de vouloir modifier cette tâche?")) {
      console.log(this.tache)
      this.tacheService.updateTache(this.tache).subscribe(data => {
      })
    } else {
      infosTache.revert();
      alert("OK OK! je ne touche à rien")
    }
  }// fin de méthode

  //METHODE EVENT

  // ngAfterViewInit(){
  //   // this.modalBg = this.elemRef.nativeElement(".modal-bg") ;
  //   //  this.modalClose = this.elemRef.nativeElement(".modal-close");

  //   //  this.modalBtn.addEventListener("click",()=>{this.modalBg.classList.add("bg-active")})
  //   //  this.modalClose.addEventListener("click",()=>{this.modalBg.classList.remove("bg-active")})

  //   }

  displayForm(infos) {

    this.end = infos.end.toISOString().replace("Z", "")
    this.start = infos.start.toISOString().replace("Z", "")
    this.modalBg.nativeElement.classList.add("bg-active");
    this.displayCalendar = false
  }

  hiddenForm() {
    this.modalBg.nativeElement.classList = "modal-bg"
    this.displayCalendar = true;
  }

  getObjectSelect(infos) {
    infos.start
    infos.end
  }

  addTache() {

    this.tacheToAdd.start = new Date(this.start)
    this.tacheToAdd.end = new Date(this.end)
    this.tacheService.addTache(this.tacheToAdd,this.titreAffaire).subscribe((data) => {
    console.log(data)
    this.taches.push(data);
    this.hiddenForm()
    })
  }

  deleteTache(infos) {

    if (confirm("Voulez-vous voir les détails de cette tâche?")) {


    // console.log (infos.event.extendedProps.idTache) 
      this.rt.navigate(['/detailsTache'],{ queryParams: { id :infos.event.extendedProps.idTache}})
      // ,{queryParams : {id : infos.event.extendedProps.idTache}}

    } else if (confirm("Voulez-vous supprimer cette tâche?")) {
      this.tacheService.deleteTache(infos.event.extendedProps.idTache).subscribe((response) => {
        location.reload();
      })
    } //else






  }// méthode




}




// TacheModel {title: "revfzfgre", start: "2020-07-24T16:59", end: "2020-07-24T16:59", description: "azsdzd"}
//2020-07-31T12:30:00.000
// description: "azsdzd"
// end: "2020-07-24T16:59"
// start: "2020-07-24T16:59"
// title: "revfzfgre"
// __proto__: Object


// {idTache: 1, start: Wed Jul 29 2020 19:00:00 GMT+0200 (heure d’été d’Europe centrale), title: "je suis la et pas toi", end: Wed Jul 29 2020 22:30:00 GMT+0200 (heure d’été d’Europe centrale), description: "", …}
// description: ""
// end: Wed Jul 29 2020 22:30:00 GMT+0200 (heure d’été d’Europe centrale) {}
// idTache: 1
// start: Wed Jul 29 2020 19:00:00 GMT+0200 (heure d’été d’Europe centrale) {}
// statutAudience: true
// title: "je suis la et pas toi"
// __proto__: Object