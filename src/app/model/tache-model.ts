import { Affaire } from './affaire';

export class TacheModel {

    idTache: number;
    start: Date;
    title: string;
    description: string;
    statutAudience: boolean;
    end:Date;
    affaire : Affaire =new Affaire();
}
